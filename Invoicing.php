<?php
namespace Invoicing;

const IVA = 0.19;

class API{
	var $service;
	public function __construct(){
		$this->service = new \SoapClient('http://ws.facturacion.cl/WSDS/wsplano.asmx?wsdl', [
			'trace' => 1
		]);
	}
	public function setLogin($username, $password, $rut, $port){
		$this->login = [
			'Usuario'		=> $username,
			'Rut'			=> $password,
			'Clave'			=> $rut,
			'Puerto'		=> $port,
			'IncluyeLink'	=> '1'
		];
		return $this;
	}
	public function getBoletaTicket($ticket){
		return $this->service->getBoletaTicket([
			'login'  => $this->login,
			'ticket' => $ticket
		]);
	}
}

class Document{
	var $IdDoc;
	var $TipoDTE;
	var $Folio;
	var $FchEmis;
	var $IndServicio;
	var $IndMntNeto;
	var $PeriodoDesde;
	var $PeriodoHasta;
	var $FchVenc;

	private $items;
	private $issuer;
	private $receiver;
	private $discount;
	private $references = [];

	public function __construct(){
		$this->items = new Items($this);
	}
	public function appendItem(Item $item){
		$this->items->append($item);
		return $this;
	}
	public function appendIssuer(Issuer $issuer){
		$this->issuer 	= $issuer;
		return $this;
	}
	public function appendReceiver(Receiver $receiver){
		$this->receiver = $receiver;
		return $this;
	}
	public function appendDiscount(Discount $discount){
		$this->discount = $discount;
		return $this;
	}
	public function appendReference(Reference $reference){
		$this->references[] = $reference;
		return $this;
	}
	public function toArray(){
		return [
			'Encabezado' => [
				'IdDoc' => [
					'TipoDTE'		=> $this->TipoDTE,
					'Folio'			=> $this->Folio,
					'FchEmis'		=> $this->FchEmis,
					'IndServicio'	=> $this->IndServicio,
					'IndMntNeto'	=> $this->IndMntNeto,
					'PeriodoDesde'	=> $this->PeriodoDesde,
					'PeriodoHasta'	=> $this->PeriodoHasta,
					'FchVenc'		=> $this->FchVenc
				],
				'Emisor'	=> $this->issuer->toArray(),
				'Receptor'	=> $this->receiver->toArray(),
				'Totales'	=> $this->items->toArrayTotal(),
			],
			'Detalle'	 	=> $this->items->toArray(),
			'DscRcgGlobal'	=> $this->discount ? $this->discount->toArray() : null,
			'Referencia' 	=> $this->references ? $this->references : null
		];
	}
}

class Issuer{
	var $RUTEmisor;
	var $RznSocEmisor;
	var $GiroEmisor;
	var $DirOrigen;
	var $CmnaOrigen;
	var $CiudadOrigen;
	public function appendTo(Document $document){
		return $document->appendIssuer($this);
	}
	public function toArray(){
		return [
			'RUTEmisor'		=> $this->RUTEmisor,
			'RznSocEmisor'	=> $this->RznSocEmisor,
			'GiroEmisor'	=> $this->GiroEmisor,
			'DirOrigen'		=> $this->DirOrigen,
			'CmnaOrigen'	=> $this->CmnaOrigen,
			'CiudadOrigen'	=> $this->CiudadOrigen
		];
	}
}

class Receiver{
	var $RUTRecep;
	var $CdgIntRecep;
	var $RznSocRecep;
	var $Contacto;
	var $DirRecep;
	var $CmnaRecep;
	var $CiudadRecep;
	public function appendTo(Document $document){
		return $document->appendReceiver($this);
	}
	public function toArray(){
		return [
			'RUTRecep'		=> $this->RUTRecep,
			'CdgIntRecep'	=> $this->CdgIntRecep,
			'RznSocRecep'	=> $this->RznSocRecep,
			'Contacto'		=> $this->Contacto,
			'DirRecep'		=> $this->DirRecep,
			'CmnaRecep'		=> $this->CmnaRecep,
			'CiudadRecep'	=> $this->CiudadRecep
		];
	}
}

class Item{
	var $NroLinDet;
	var $TpoCodigo;
	var $VlrCodigo;
	var $CdgItem;
	var $NmbItem;
	var $DscItem;
	var $QtyItem;
	var $UnmdItem;
	var $PrcItem;
	var $MontoItem;
	var $IndExe;
	public function getTotal(){
		return $this->MontoItem = ($this->PrcItem * $this->QtyItem);
	}
	public function appendTo(Document $document){
		return $document->appendItem($this);
	}
	public function toArray(){
		return [
			'NroLinDet'	=> $this->NroLinDet,
			'CdgItem'	=> [
				'TpoCodigo'	=> $this->TpoCodigo,
				'VlrCodigo'	=> $this->VlrCodigo
			],
			'CdgItem'	=> $this->CdgItem,
			'NmbItem'	=> $this->NmbItem,
			'DscItem'	=> $this->DscItem,
			'QtyItem'	=> $this->QtyItem,
			'UnmdItem'	=> $this->UnmdItem,
			'PrcItem'	=> $this->PrcItem,
			'MontoItem'	=> $this->getTotal()
		];
	}
}

class Items{
	var $MntNeto 		= 0;
	var $MntExe 		= 0;
	var $IVA 			= 0;
	var $MntTotal		= 0;
	var $TotalPeriodo	= 0;
	var $SaldoAnterior	= 0;
	var $VlrPagar		= 0;
	var $MontoNF		= 0;

	private $items;

	public function __construct(Document $document){
		$this->document = $document;
	}
	public function append(Item $item){
		$this->items[]  = $item;

		if($item->IndExe == 1)
			$this->MntExe  += $item->getTotal();
		else
			$this->MntNeto += $item->getTotal();
	}
	public function getIva(){
		return $this->IVA = ($this->MntExe * IVA);
	}
	public function getTotal(){
		if($this->document->IndMntNeto == 2)
			return $this->MntTotal = ($this->getIva() + $this->MntNeto + $this->MntExe);
		else
			return $this->MntTotal = $this->MntNeto;
	}
	public function getTotalPeriod(){
		return ($this->MntTotal + $this->MontoNF);
	}
	public function getPaymentAmount(){
		return $this->VlrPagar = ($this->getTotal() + $this->SaldoAnterior);
	}
	public function toArray(){
		$items = [];
		foreach($this->items as $item){
			$items[] = $item->toArray();
		}
		return $items;
	}
	public function toArrayTotal(){
		return [
			'MntNeto'		=> $this->MntNeto,
			'MntExe'		=> $this->MntExe,
			'IVA'			=> $this->getIva(),
			'MntTotal'		=> $this->getTotal(),
			'TotalPeriodo'	=> $this->getTotalPeriod(),
			'SaldoAnterior' => $this->SaldoAnterior,
			'VlrPagar'		=> $this->getPaymentAmount()
		];
	}
}

class Discount{
	var $NroLinDR;
	var $TpoMov;
	var $GlosaDR;
	var $TpoValor;
	var $ValorDR;
	var $IndExeDR;
	public function appendTo(Document $document){
		return $document->appendDiscount($this);
	}
	public function toArray(){
		return [
			'NroLinDR'	=> $this->NroLinDR,
			'TpoMov'	=> $this->TpoMov,
			'GlosaDR'	=> $this->GlosaDR,
			'TpoValor'	=> $this->TpoValor,
			'ValorDR'	=> $this->ValorDR,
			'IndExeDR'	=> $this->IndExeDR
		];
	}
}

class Reference{
	var $NroLinRef;
	var $CodRef;
	var $RazonRef;
	public function appendTo(Document $document){
		return $document->appendReference($this);
	}
	public function toArray(){
		return [
			'NroLinRef'	=> $this->NroLinRef,
			'CodRef'	=> $this->CodRef,
			'RazonRef'	=> $this->RazonRef
		];
	}
}